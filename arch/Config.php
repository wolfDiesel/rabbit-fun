<?php

namespace arch;

class Config
{
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function __get($name)
    {
        if (key_exists($name, $this->config)) {
            return $this->config[$name];
        } else {
            return null;
        }
    }

    public function __set($name, $value)
    {
        $this->config[$name] = $value;
    }
}