<?php

namespace arch;

use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Wire\AMQPTable;

class Consumer implements IRabbitUnit
{

    private $config;
    private $connection;
    private $channel;
    private $qParams;

    public function __construct(AMQPStreamConnection $connection, Config $config)
    {
        $this->connection = $connection;
        $this->config = $config;
        $this->channel = $connection->channel();
        $this->prepare();
    }

    function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }

    protected function prepare()
    {
        try {

            $this->channel->exchange_declare($this->config->exchangeName, $this->config->exchangeType, false, $this->config->exchangeDurable, false);
            list($qname, ,) = $this->channel->queue_declare($this->config->queueName, false, $this->config->durable, $this->config->queueExclusive, false, false, $this->qParams);
            $this->channel->queue_bind($qname, $this->config->exchangeName);
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            echo $e->getLine() . PHP_EOL;
            echo $e->getFile() . PHP_EOL;
        }
    }

    public function run(callable $callback = null)
    {

        $this->channel->basic_consume($this->config->queueName, '', false, $this->config->durable, false, false, (($callback) ? $callback : [$this, 'work']), null);
        while (count($this->channel->callbacks)) {
            if ($this->config->consumeUpperTimer) {
                sleep(rand(1, $this->config->consumeUpperTimer));
            }
            $this->channel->wait();
        }
    }

    function work($msg)
    {
        echo ' [x] ', $msg->body, "\n";
    }
}