<?php

namespace arch;

use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class ConsumerOne implements IRabbitUnit
{

    protected $config;
    protected $connection;
    protected $channel;
    protected $qParams;

    public function __construct(AMQPStreamConnection $connection, Config $config)
    {
        $this->connection = $connection;
        $this->config = $config;
        $this->channel = $connection->channel();
        $this->prepare();
    }

    function __destruct()
    {
        //$this->channel->close();
        //$this->connection->close();
    }

    protected function prepare()
    {
        try {
            $this->qParams = new AMQPTable();
            if ($this->config->ttl) {
                $this->qParams->set("x-message-ttl", $this->config->ttl * 1000);
            }
            if ($this->config->expires) {
                $this->qParams->set("x-expires", $this->config->ttl * 1000);
            }
            if ($this->config->exchangeName) {
                $this->channel->exchange_declare($this->config->exchangeName, $this->config->exchangeType, false, $this->config->echangeDurable, false);
            }
            list($qname, ,) = $this->channel->queue_declare($this->config->queueName, false, $this->config->durable, $this->config->queueExclusive, false,false, $this->qParams);
            $this->channel->queue_bind($this->config->queueName, $this->config->exchangeName, $this->config->queueName);
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            echo $e->getLine() . PHP_EOL;
            echo $e->getFile() . PHP_EOL;
        }
    }

    public function run(callable $callback = null)
    {
        /** @var AMQPMessage $got */
        $got = $this->channel->basic_get($this->config->queueName, true);
        if ($got) {
            //$this->channel->basic_nack()
            //$this->channel->basic_ack($got->delivery_info['delivery_tag']);
            if ($callback) {
                return $callback($got);
            } else {
                return $got->getBody();
            }
        }
    }

    /** @param AMQPMessage $msg */
    function work($msg)
    {
        echo ' [x] ', $msg->body, "\n";
    }

}