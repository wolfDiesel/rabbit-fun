<?php
/**
 * Created by PhpStorm.
 * User: wolf
 * Date: 30.12.2017
 * Time: 14:56
 */

namespace arch;


interface IDaemonRunner
{
    const THREADS_LIMIT = 20;
    const UPDATE_TIMER = 1;
    public function run();
    public function addConfig(Config $config);
}