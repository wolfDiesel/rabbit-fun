<?php
namespace arch;
use PhpAmqpLib\Connection\AMQPStreamConnection;

interface IRabbitUnit
{
    public function __construct(AMQPStreamConnection $connection, Config $config);
    public function run(callable $callback = null);
    function work($msg);
}