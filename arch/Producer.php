<?php

namespace arch;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Producer implements IRabbitUnit
{
    protected $config;
    protected $connection;
    protected $channel;

    public function __construct(AMQPStreamConnection $connection, Config $config)
    {
        $this->connection = $connection;
        $this->config = $config;
        $this->channel = $connection->channel();
        $this->init();
    }

    function __destruct()
    {
        if (!$this->config->durable) {
            $this->channel->exchange_delete($this->config->exchangeName);
        }
    }


    public function init()
    {
        $this->channel->exchange_declare($this->config->exchangeName, $this->config->exchangeType, false, $this->config->durable, false);
        if ($this->config->queueName) {
            list($qname, ,) = $this->channel->queue_declare($this->config->queueName, false, $this->config->durable, $this->config->queueExclusive, false);
            $this->channel->queue_bind($qname, $this->config->exchangeName, '', false);
        }
    }

    public function run(callable $callback = null)
    {
        while (true) {
            $this->work(" message : " . rand(100, 10000));
        }
    }

    function work($msg)
    {
        $i = 0;
        while (true) {
            $messageId = ' [Message ID]: ' . rand(100, 1000);
            if (empty($data)) $data = "info: Message Number: ";
            $msg = new AMQPMessage($data . $messageId);
            $this->channel->basic_publish($msg, $this->config->exchangeName, '', $this->config->durable);
            echo " [x] Sent ", $data . $messageId, "\n";
            if ($i == 25) {
                $i = 0;
                if ($this->config->producerUpperTimer) {
                    sleep(rand(1, $this->config->producerUpperTimer));
                }
                sleep(20);
            }
            $i++;
        }
    }

}