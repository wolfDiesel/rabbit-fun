<?php

namespace arch;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class ProducerOne implements IRabbitUnit
{
    protected $config;
    protected $connection;
    protected $channel;
    protected $qParams;

    public function __construct(AMQPStreamConnection $connection, Config $config)
    {
        $this->connection = $connection;
        $this->config = $config;
        $this->channel = $connection->channel();
        $this->init();
    }

    function __destruct()
    {
        if (!$this->config->durable) {
            $this->channel->exchange_delete($this->config->exchangeName);
        }
    }

    public function init()
    {
        $this->qParams = new AMQPTable();
        if ($this->config->ttl) {
            $this->qParams->set("x-message-ttl", $this->config->ttl * 1000);
        }
        if ($this->config->expires) {
            $this->qParams->set("x-expires", $this->config->ttl * 1000);
        }
        if ($this->config->exchangeName) {
            $this->channel->exchange_declare($this->config->exchangeName, $this->config->exchangeType, false, $this->config->exchangeDurable, false);
        }
        if ($this->config->exchangeName) {
            list($qname, ,) = $this->channel->queue_declare($this->config->queueName, false, $this->config->durable, $this->config->queueExclusive, false, false, $this->qParams);
            if($this->config->bind_ahead==true){
                $this->channel->queue_bind($qname, $this->config->exchangeName, $qname, false);
            }
        }
    }

    public function run(callable $callback = null)
    {
        $this->work(" message : " . rand(100, 10000));
    }

    function work($msg)
    {
        $messageId = ' [Message ID]: ' . rand(100, 1000);
        if (empty($data)) $data = "info: Message Number: ";
        $msg = new AMQPMessage($data . $messageId);
        $this->channel->basic_publish($msg, $this->config->exchangeName, $this->config->queueName, $this->config->durable, null);
        echo " [x] Produced ", $data . $messageId, "\n";
    }

}