<?php

namespace arch;

class RabbitRunner
{
    protected $unit;
    public function __construct(IRabbitUnit $rabbitUnit)
    {
        $this->unit = $rabbitUnit;
    }

    public function run(callable $callback = null)
    {
        $this->unit->run($callback);
    }
}