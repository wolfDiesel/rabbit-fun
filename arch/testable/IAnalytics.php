<?php

namespace RaFun\arch\testable;


interface IAnalytics
{
    public function getProcessAllowedInfo(array $commands);
}