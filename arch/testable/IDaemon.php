<?php

namespace arch\testable;
/**
 * Interface IDaemon
 * @package arch\testable
 */
interface IDaemon
{
    const UPDATE_TIMER = 1;


    /**
     * Takes a callable procedure and wraps it into daemon process
     * @param callable $callback
     * @return mixed
     */
    public function daemonize(callable $callback);

}