<?php
/**
 * Created by PhpStorm.
 * User: wolf
 * Date: 17.01.18
 * Time: 14:15
 */

namespace RaFun\arch\testable;


interface ILog
{

    public function pushLog(string $logString);

}