<?php

namespace RaFun\arch\testable;


class PlainAnalytics implements IAnalytics
{

    /**
     * Checks if command started and retirns its pids
     * @param string $command command that should be tested
     * @return []
     */
    protected function checkCommandStarted($command)
    {
        $pid = [];
        exec('ps aux | grep "' . $command . '$" | grep -v grep | grep -v emacs', $pid);
        return [!empty($pid), $pid];
    }

    /**
     * Checks if command started and retirns its pids
     * @param array $commands command that should be tested
     * @return []
     */
    public function getProcessAllowedInfo(array $commands)
    {
        $ret = [];
        foreach ($commands as $command => $limit) {
            list($is_run, $pids) = $this->checkCommandStarted($command);
            $ret[$command] = ($limit - count($pids));
        }
        return $ret;
    }
}