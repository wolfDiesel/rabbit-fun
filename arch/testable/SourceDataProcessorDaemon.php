<?php

namespace RaFun\arch\testable;


use arch\testable\IDaemon;

class SourceDataProcessorDaemon implements IDaemon
{

    /**
     * Takes a callable procedure and wraps it into daemon process
     * @param callable $callback
     * @return mixed
     */
    public function daemonize(callable $callback)
    {
        // TODO: Implement daemonize() method.
    }
}