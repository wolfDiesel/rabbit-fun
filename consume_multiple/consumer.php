<?php

require_once __DIR__ . '/../vendor/autoload.php';

use arch\Config;
use arch\RabbitRunner;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$conf1 = new Config(['queueName' => 'lgs1', 'queueExclusive' => false, 'exchangeName' => 'LogsDirect', 'exchangeType' => 'direct', 'durable'=>true, 'consumeUpperTimer' => 2]);
$conf2 = new Config(['queueName' => 'lgs2', 'queueExclusive' => false, 'exchangeName' => 'LogsDirect', 'exchangeType' => 'direct', 'durable'=>true, 'consumeUpperTimer' => 2]);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

$cons1 = new \arch\ConsumerOne($connection, $conf1);
$cons2 = new \arch\ConsumerOne($connection, $conf2);

$runner1 = new RabbitRunner($cons1);
$runner2 = new RabbitRunner($cons2);

while(1==1){
    $runner1->run([$cons1, 'work']);
    $runner2->run([$cons2, 'work']);
}