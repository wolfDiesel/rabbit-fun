<?php
require_once __DIR__ . '/../vendor/autoload.php';
use arch\Config;
use arch\Producer;
use arch\RabbitRunner;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$conf1 = new Config(['queueName' => 'lgs1', 'queueExclusive' => false, 'exchangeName' => 'LogsDirect', 'exchangeType' => 'direct', 'durable' => true, 'producerUpperTimer' => 5]);
$conf2 = new Config(['queueName' => 'lgs2', 'queueExclusive' => false, 'exchangeName' => 'LogsDirect', 'exchangeType' => 'direct', 'durable' => true, 'producerUpperTimer' => 5]);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

$runner1 = new RabbitRunner(new \arch\ProducerOne($connection, $conf1));
$runner2 = new RabbitRunner(new \arch\ProducerOne($connection, $conf2));

$i = 0;
while(1==1){
    $runner1->run();
    $runner2->run();
    if($i==20){
        sleep(60);
        $i=0;
    }
    $i++;
}