<?php

require_once __DIR__ . '/../vendor/autoload.php';

use arch\Config;
use arch\Consumer;
use arch\RabbitRunner;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$conf = new Config(['queueName' => 'LogsQueque', 'queueExclusive' => false, 'exchangeName' => 'LogsDirect', 'exchangeType' => 'direct']);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

$runner = new RabbitRunner(new Consumer($connection, $conf));
$runner->run();