<?php

require_once __DIR__ . '/../vendor/autoload.php';

use arch\Config;
use arch\ConsumerOne;
use arch\RabbitRunner;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$conf = new Config(['queueName' => 'LogsQueque', 'queueExclusive' => false, 'exchangeName' => 'LogsDirect', 'exchangeType' => 'direct', 'durable'=>true, 'consumeUpperTimer' => 2]);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

$x = function ($msg){
   echo "Called back with message: " . $msg . PHP_EOL;
};

$runner = new RabbitRunner(new ConsumerOne($connection, $conf));
$runner->run($x);