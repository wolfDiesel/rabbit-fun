<?php
require_once __DIR__ . '/../vendor/autoload.php';
use arch\Config;
use arch\Producer;
use arch\RabbitRunner;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$conf = new Config(['queueName' => 'LogsQueque', 'queueExclusive' => false, 'exchangeName' => 'LogsDirect', 'exchangeType' => 'direct', 'durable' => true, 'producerUpperTimer' => 5]);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

$runner = new RabbitRunner(new Producer($connection, $conf));

$runner->run();