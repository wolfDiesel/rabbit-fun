Чисто скопипащено для проверки очереди "**fanout**" для кролика

Запускаем **два** consumer.php

Запускаем один producer.php

Обратить внимание: 

`list($queue_name, ,) = $channel->queue_declare($queueName, false, false, $queueExclusiveAccess, false);`

параметр 

`$queueExclusiveAccess` четко указывает, что очередь не должна блокироваться, чтобы остальные потребители могли вынимать из нее сообщения.