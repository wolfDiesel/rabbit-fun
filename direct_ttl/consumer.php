<?php

require_once __DIR__ . '/../vendor/autoload.php';

use arch\Config;
use arch\RabbitRunner;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$conf = new Config(['queueName' => 'ttl1', 'queueExclusive' => false, 'exchangeName' => 'TestDirectTTL', 'exchangeType' => 'direct', 'ttl' => 1]);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

$runner = new RabbitRunner(new \arch\ConsumerOne($connection, $conf));
while(1==1){
    sleep(3);
    $runner->run();
}
