<?php
require_once __DIR__ . '/../vendor/autoload.php';
use arch\Config;
use arch\RabbitRunner;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$conf = new Config(['queueName' => 'ttl1', 'queueExclusive' => false, 'exchangeName' => 'TestDirectTTL', 'exchangeType' => 'direct', 'ttl' => 15]);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

$runner1 = new RabbitRunner(new \arch\ProducerOne($connection, $conf));

$i = 0;
while (1 == 1) {
    $runner1->run();
    if ($i == 100) {
        sleep(30);
        $i = 0;
    }
    $i++;
}