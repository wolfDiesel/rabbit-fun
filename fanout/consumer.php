<?php

require_once __DIR__ . '/../vendor/autoload.php';

use arch\Config;
use arch\Consumer;
use arch\RabbitRunner;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$conf = new Config([
    'queueName' => '',
    'queueExclusive' => false,
    'exchangeName' => 'LogsFanout',
    'exchangeType' => 'fanout',
    'consumeUpperTimer' => false,
    'durable' => true
]);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

$runner = new RabbitRunner(new Consumer($connection, $conf));
