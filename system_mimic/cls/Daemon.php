<?php

namespace system_mimic\cls;

use arch\Config;
use arch\IDaemonRunner;

class Daemon implements IDaemonRunner
{
    protected $configs=[];
    protected $loop;
    protected $threadsLimit;

    public function run()
    {
        $this->loop = \React\EventLoop\Factory::create();
        $this->loop->addPeriodicTimer(self::UPDATE_TIMER, function () {
            foreach ($this->configs as $config){
                list($commandStarted, $pids) = $this->checkCommandStarted($config->command);
                if (count($pids) < $config->threadsLimit) {
                    for ($i = 1; $i <= $config->threadsLimit; $i++) {
                        exec("nohup {$config->command} > /dev/null 2>&1 &");
                        echo "nohup {$config->command} > /dev/null 2>&1 &" . PHP_EOL;
                    }
                }
            }

        });
        $this->loop->run();
    }

    function checkCommandStarted($command)
    {
        $pid = [];
        exec('ps aux | grep "' . $command . '$" | grep -v grep | grep -v emacs', $pid);
        return [!empty($pid), $pid];
    }

    public function addConfig(Config $config)
    {
        $this->configs[] = $config;
    }
}