<?php

namespace system_mimic\cls;

use arch\ProducerOne;
use PhpAmqpLib\Message\AMQPMessage;

class ParserDynamicProducer extends ProducerOne
{
    const STATE_ENABLED = 1;
    const STATE_DISABLED = 0;
    const STATE_DRIVING = 2;
    protected $states = [
        self::STATE_DISABLED,
        self::STATE_ENABLED,
        self::STATE_DRIVING
    ];

    function work($msg)
    {
        $json = [];
        if (strstr( $this->config->queueName, 'trip') !== false) {
            $json = [
                'queueName' => $this->config->queueName,
                'timeProduced' => time(),
                'fare' => rand(0,1000),
                'commission' => rand(0,1000),
                'trip_id' => rand(0,100000000000),
                'source_id' => $this->config->source_id,
            ];
        } elseif (strstr($this->config->queueName,'state') !== false) {
            $json = [
                'queueName' => $this->config->queueName,
                'timeProduced' => time(),
                'state' => $this->states[rand(0, (count($this->states)-1))],
                'source_id' => $this->config->source_id,
            ];
        }

        $json = json_encode($json);
        $msg = new AMQPMessage($json);
        $this->channel->basic_publish($msg, $this->config->exchangeName, $this->config->queueName, $this->config->durable, null);
        echo " [x] Sent bytes: ", strlen($json), "\n";
    }
}