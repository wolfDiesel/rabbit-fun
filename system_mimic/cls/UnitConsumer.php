<?php

namespace system_mimic\cls;

use arch\ConsumerOne;

class UnitConsumer extends ConsumerOne
{
    public function work($msg)
    {
        $a = json_decode($msg->getBody(), true);
        if ($a) {
            $a['unitTag'] = $this->config->queueName;
            $a['unitTime'] = time();
        }
        return json_encode($a);
    }
}