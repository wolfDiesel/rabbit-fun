<?php

namespace system_mimic;
require_once __DIR__ . '/../vendor/autoload.php';

use \arch\Config;
use system_mimic\cls\Daemon;

$commands = [
    new Config([
        'threadsLimit' => 1,
        'command' => "php " . __DIR__ . "/parserLevelDynamic.php"
    ]),
    new Config([
        'threadsLimit' => 1, 
        'command' => "php " . __DIR__ . "/unitLevel.php"
    ])

];

$daemon = new Daemon();
foreach ($commands as $name => $command) {
    $daemon->addConfig($command);
}

$daemon->run();