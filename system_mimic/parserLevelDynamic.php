<?php


//here we'll need producer only

namespace system_mimic;
require_once __DIR__ . '/../vendor/autoload.php';
use arch\Config;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use system_mimic\cls\ParserDynamicProducer;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');


$productorConfigs = [
    new Config([
        'queueName' => 'source.1.state.queue',
        'source_id' => 'yandex',
        'queueExclusive' => false,
        'exchangeName' => 'DynamicData',
        'exchangeType' => 'direct',
        'durable' => false,
        'ttl' => 160
    ]),
    new Config([
        'queueName' => 'source.1.trip.queue',
        'source_id' => 'yandex',
        'queueExclusive' => false,
        'exchangeName' => 'DynamicData',
        'exchangeType' => 'direct',
        'durable' => true,
    ]),
    new Config([
        'queueName' => 'source.uuid-12312.state.queue',
        'source_id' => 'uber',
        'queueExclusive' => false,
        'exchangeName' => 'DynamicData',
        'exchangeType' => 'direct',
        'durable' => true,
    ]),
    new Config([
        'queueName' => 'source.uuid-12312.trip.queue',
        'source_id' => 'uber',
        'queueExclusive' => false,
        'exchangeName' => 'DynamicData',
        'exchangeType' => 'direct',
        'durable' => true,
    ])
];

$productors = [];
foreach ($productorConfigs as $productorConfig) {
    $productors[] = new ParserDynamicProducer($connection, $productorConfig);
}

$i=0;
while (1 == 1) {
    foreach ($productors as $productor) {
        $productor->run();
    }
    if($i==100){
        sleep(5);
        $i=0;
    }
    $i++;
}