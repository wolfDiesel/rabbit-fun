<?php


//here we'll need producer only

namespace system_mimic;
require_once __DIR__ . '/../vendor/autoload.php';
use arch\Config;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use system_mimic\cls\UnitConsumer;
use system_mimic\cls\ParserDynamicProducer;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');


$unitConfigs = [
    new Config([
        'queueName' => 'source.1.state.queue',
        'queueExclusive' => false,
        'exchangeName' => 'DynamicData',
        'exchangeType' => 'direct',
        'durable' => false,
        'ttl' => 160
    ]),
    new Config([
        'queueName' => 'source.1.trip.queue',
        'queueExclusive' => false,
        'exchangeName' => 'DynamicData',
        'exchangeType' => 'direct',
        'durable' => true,
    ]),
    new Config([
        'queueName' => 'source.uuid-12312.state.queue',
        'queueExclusive' => false,
        'exchangeName' => 'DynamicData',
        'exchangeType' => 'direct',
        'durable' => true,
    ]),
    new Config([
        'queueName' => 'source.uuid-12312.trip.queue',
        'queueExclusive' => false,
        'exchangeName' => 'DynamicData',
        'exchangeType' => 'direct',
        'durable' => true,
    ]),
];

$units = [];
foreach ($unitConfigs as $unitConfig) {
    $units[] = new UnitConsumer($connection, $unitConfig);
}

$i = 0;
while (1 == 1) {
    foreach ($units as $unit) {
        $mtime = microtime(true);
        $unit->run();
        echo microtime(true) - $mtime . PHP_EOL;

    }
}