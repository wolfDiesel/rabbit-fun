<?php

namespace RaFun\testable_code;
use arch\testable\IDaemon;

class Daemon implements IDaemon
{
    /**
     * Takes a callable procedure and wraps it into daemon process
     * @param callable $callback
     * @return mixed
     */
    public function daemonize(callable $callback)
    {
    }
}