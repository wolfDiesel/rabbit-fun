<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;
    protected $processLimit = 0;
    protected $thecommand = "php " . __DIR__ . '/../_data/test_command.php';
    protected $reflection = null;

    /**
     * @Given i have :num1 processes run
     */
    public function iHaveProcessesRun($num1)
    {
        $this->amGoingTo("Test {$num1} processes");
        for ($i = 1; $i <= $num1; $i++) {
            $this->runSpecifiedCommand($this->thecommand);
        }
    }

    /**
     * @When i see :num1 process
     */
    public function iSeeProcess($num1)
    {
        $this->reflection = new \ReflectionClass('RaFun\arch\testable\PlainAnalytics');
        $method = $this->reflection->getMethod("checkCommandStarted");
        $method->setAccessible(true);
        list($is_run, $pids) = $method->invokeArgs($this->reflection->newInstance(), [$this->thecommand]);
        $this->assertTrue($is_run);
        $this->assertNotEmpty($pids);
        $this->assertEquals(count($pids), $num1);
    }


    /**
     * @When i test with limit :num1
     */
    public function iTestWithLimit($num1)
    {
        $this->processLimit = $num1;
    }

    /**
     * @Then i should see :num1 slots
     */
    public function iShouldSeeSlots($num1)
    {
        $this->amGoingTo("Expect {$num1} processes");
        $anal = new \RaFun\arch\testable\PlainAnalytics();
        $result = $anal->getProcessAllowedInfo([$this->thecommand => $this->processLimit]);
        $this->assertEquals([$this->thecommand => $num1], $result);
    }

    /**
     * @Then i cleanup
     */
    public function iCleanup()
    {
        exec("pkill -f '{$this->thecommand}'");
    }


}
