<?php
/**
 * Created by Wolf Diesel.
 * User: diesel.wolf@yandex.ru
 * Date: 17.01.18
 */

namespace Helper;

class SystemProcessHelper extends \Codeception\Module
{

    public $backgorundPrefix = "nohup ";
    public $backgroungPostfix = "> /dev/null 2>&1 &";

    /**
     * Checks if command started and retirns its pids
     * @param string $command  command that should be tested
     * @return []
     */
    public function checkCommandStarted($command)
    {
        $pid = [];
        exec('ps aux | grep "' . $command . '$" | grep -v grep | grep -v emacs', $pid);
        return [!empty($pid), $pid];
    }

    /**
     * Runs a specified command
     * @param string $command command that should be fired up
     * @param bool $nohup shows if it should be ran in background mode
     * @return integer
     */
    public function runSpecifiedCommand($command, $nohup = true)
    {
        if($nohup){
            $command = $this->backgorundPrefix . $command . $this->backgroungPostfix;
        }
        return exec($command);
    }



}