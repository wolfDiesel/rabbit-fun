Feature: checkout
  In order to check PlainAnalytics
  As a Tester
  I need to get number of processes a can start

  Scenario: check with 2 process 4 limit
    Given i have 2 processes run
    When i see 2 process
    And i test with limit 4
    Then i should see 2 slots
    Then i cleanup

  Scenario: check with 4 process 4 limit
    Given i have 4 processes run
    When i see 4 process
    And i test with limit 4
    Then i should see 0 slots
    Then i cleanup

  Scenario: check with 5 process 4 limit
    Given i have 5 processes run
    When i see 5 process
    And i test with limit 4
    Then i should see "-1" slots
    Then i cleanup
