<?php


class PlainAnanlyticsTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $processLimit = 0;
    protected $thecommand = "php " . __DIR__ . '/../_data/test_command.php';
    /** @var \Reflection */
    protected $reflection = null;

    protected function _before()
    {
        $this->reflection = new \ReflectionClass('RaFun\arch\testable\PlainAnalytics');
    }

    protected function _after()
    {
        exec("pkill -f '{$this->thecommand}'");
    }

    protected function iHaveProcessesRun($num1)
    {
        for ($i = 1; $i <= $num1; $i++) {
            $this->tester->runSpecifiedCommand($this->thecommand);
        }
    }

    public function testPositiveSlotsValue()
    {
        $this->processLimit = 4;
        $this->iHaveProcessesRun(2);
        $anal = new \RaFun\arch\testable\PlainAnalytics();
        $result = $anal->getProcessAllowedInfo([$this->thecommand => $this->processLimit]);
        $this->assertEquals([$this->thecommand => 2], $result);
    }

    public function testNegativeSlotsValue()
    {
        $this->processLimit = 4;
        $this->iHaveProcessesRun(5);
        $anal = new \RaFun\arch\testable\PlainAnalytics();
        $result = $anal->getProcessAllowedInfo([$this->thecommand => $this->processLimit]);
        $this->assertEquals([$this->thecommand => -1], $result);
    }

    public function testZeroeSlotsValue()
    {
        $this->processLimit = 4;
        $this->iHaveProcessesRun(4);
        $anal = new \RaFun\arch\testable\PlainAnalytics();
        $result = $anal->getProcessAllowedInfo([$this->thecommand => $this->processLimit]);
        $this->assertEquals([$this->thecommand => 0], $result);
    }

    public function testCheckCommandStarted()
    {
        $this->iHaveProcessesRun(2);
        $method = $this->reflection->getMethod("checkCommandStarted");
        $method->setAccessible(true);
        list($is_run, $pids) = $method->invokeArgs($this->reflection->newInstance(), [$this->thecommand]);
        $this->assertTrue($is_run);
        $this->assertNotEmpty($pids);
        $this->assertEquals(count($pids), 2);
    }

}